#include "Layer.hpp"

#include <iostream>
#include <random>

namespace {
Matrix append_row(const Matrix& matrix, const Vector& v);
void apply_sigmoid(Matrix& matrix);
}

Layer::Layer(unsigned n_neurons, unsigned n_next_layer_neurons) noexcept
{
    static std::random_device dev;
    static std::mt19937 generator(dev());
    static std::normal_distribution<double> distribution(0, 1);
    auto uniform = [&]() { return distribution(generator); };

    W = Matrix::NullaryExpr(n_next_layer_neurons, n_neurons + 1, uniform);
}

Matrix Layer::forward_pass(const Matrix& previous_W,
                           const Matrix& previous_Y) noexcept
{
    Y = previous_W * previous_Y;
    apply_sigmoid(Y);
    // Returns y with appended ones.
    return append_row(Y, Vector::Constant(Y.cols(), 1.0));
}

Matrix Layer::backward_pass(const Matrix& next_delta, double eta) noexcept
{
    auto delta = calculate_delta(next_delta);
    update_weights(next_delta, eta);

    return delta;
}

const Matrix& Layer::get_W() const noexcept
{
    return W;
}

Matrix Layer::get_dummy_Y() const noexcept
{
    return append_row(Y, Vector::Constant(Y.cols(), 1.0));
}

const Matrix& Layer::get_Y() const noexcept
{
    return Y;
}

void Layer::set_Y(Matrix Y) noexcept
{
    // apply_sigmoid(Y);
    this->Y = std::move(Y);
}

Matrix& Layer::get_W() noexcept
{
    return W;
}

Matrix Layer::calculate_delta(const Matrix& next_delta) const noexcept
{
    auto ones = Matrix::Constant(Y.rows(), Y.cols(), 1.0);
    return Y.cwiseProduct(ones - Y).cwiseProduct(
        get_W_without_bias().transpose() * next_delta);
}

void Layer::update_weights(const Matrix& next_delta, double eta) noexcept
{
    W += eta * next_delta * get_dummy_Y().transpose();
}

Matrix Layer::get_W_without_bias() const noexcept
{
    return W.block(0, 0, W.rows(), W.cols() - 1);
}

Matrix FinalLayer::backward_pass(const Matrix& T) const noexcept
{
    return calculate_delta(T);
}

const Matrix& FinalLayer::forward_pass(const Matrix& previous_W,
                                       const Matrix& previous_Y) noexcept
{
    Y = previous_W * previous_Y;
    apply_sigmoid(Y);
    return Y;
}

Matrix FinalLayer::calculate_delta(const Matrix& T) const noexcept
{
    auto ones = Matrix::Constant(T.rows(), T.cols(), 1.0);
    return Y.cwiseProduct(ones - Y).cwiseProduct(T - Y);
}

namespace {
Matrix append_row(const Matrix& matrix, const Vector& v)
{
    Matrix ap_matrix = matrix;
    ap_matrix.conservativeResize(matrix.rows() + 1, matrix.cols());
    ap_matrix.row(ap_matrix.rows() - 1) = v;
    return ap_matrix;
}

void apply_sigmoid(Matrix& matrix)
{
    matrix = ((-matrix).array().exp() + 1.0).cwiseInverse();
}
}
