#include "Point.hpp"

Point::Point(const wxPoint& point) : x(point.x), y(point.y) {}

Point::Point(double x, double y) : x(x), y(y) {}

Point Point::operator+=(const Point& other)
{
    *this = *this + other;
    return *this;
}

Point Point::operator-=(const Point& other)
{
    *this = *this - other;
    return *this;
}

Point Point::operator/=(const Point& other)
{
    *this = *this / other;
    return *this;
}

Point Point::operator/=(std::size_t num)
{
    *this = *this / num;
    return *this;
}

// FIXME
std::ostream& operator<<(std::ostream& os, const Point& p)
{
    return os << p.x << " " << p.y << " ";
}

Point operator+(const Point& first, const Point& other)
{
    return {first.x + other.x, first.y + other.y};
}

Point operator-(const Point& first, const Point& other)
{
    return {first.x - other.x, first.y - other.y};
}

Point operator/(const Point& first, const Point& other)
{
    return {first.x / other.x, first.y / other.y};
}

Point operator/(const Point& point, std::size_t num)
{
    return {point.x / num, point.y / num};
}

std::ostream& operator<<(std::ostream& os, const std::vector<Point>& p)
{
    for (const auto& point : p) { os << point << " "; }
    return os;
}
