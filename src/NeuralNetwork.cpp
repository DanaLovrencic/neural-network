#include "NeuralNetwork.hpp"

#include <iostream>
#include <exception>

namespace {
void verify(const std::vector<unsigned>& architecture);
}

NeuralNetwork::NeuralNetwork(const std::vector<unsigned>& architecture,
                           std::atomic<bool>& should_terminate_training)
    : should_terminate_training(should_terminate_training)
{
    set_architecture(architecture);
}

void NeuralNetwork::set_architecture(const std::vector<unsigned>& architecture)
{
    verify(architecture);
    layers.reserve(architecture.size() - 1);

    for (auto it = architecture.begin(); it != architecture.end() - 1; ++it) {
        auto curr_dim = *it;
        auto next_dim = *(it + 1);

        layers.emplace_back(curr_dim, next_dim); // +1 for bias
    }
}

Matrix NeuralNetwork::predict(const Matrix& X)
{
    forward_pass(X);
    return final_layer.Y;
}

void NeuralNetwork::train(const std::vector<Matrix>& Xs,
                          const std::vector<Matrix>& Ts,
                          double eta,
                          std::size_t n_iters,
                          std::size_t epsilon)
{
    if (Xs.size() != Ts.size()) {
        throw std::runtime_error(
            "Samples and targets should have same dimensions");
    }

    std::cout << "--------- TRAINING --------\n";
    double err = std::numeric_limits<double>::max();
    for (std::size_t i = 0; i < n_iters && err > epsilon; ++i) {
        for (std::size_t j = 0; j < Xs.size(); ++j) {
            forward_pass(Xs[j]);
            backward_pass(Ts[j], eta);
            err = (Ts[j] - final_layer.Y).array().pow(2).sum();
        }
        if (!(i % 10000)) { std::cout << "Error: " << err << '\n'; }
        if (should_terminate_training) {
            std::cout << "Training terminated!\n";
            return;
        }
    }
    std::cout << "---------------------------\n\n";
}

/* Initialize output values for all layers. */
void NeuralNetwork::forward_pass(const Matrix& X) noexcept
{
    layers.front().set_Y(X);

    auto X_dummy = layers.front().get_dummy_Y();
    auto W       = layers.front().get_W();

    for (auto it = layers.begin() + 1; it != layers.end(); ++it) {
        auto& curr = *it;

        X_dummy = curr.forward_pass(W, X_dummy);
        W       = curr.get_W();
    }

    final_layer.forward_pass(layers.back().get_W(), X_dummy);
}

/* Calculate errors (and update weights). */
void NeuralNetwork::backward_pass(const Matrix& T, double eta) noexcept
{
    Matrix next_delta = final_layer.backward_pass(T);

    for (std::size_t i = layers.size() - 1; i > 0; --i) {
        next_delta = layers[i].backward_pass(next_delta, eta);
    }
}

namespace {
void verify(const std::vector<unsigned>& architecture)
{
    if (architecture.size() < 2) {
        throw std::runtime_error(
            "Neural network should have at least two layers.");
    }
}
}
