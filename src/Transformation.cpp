#include "Transformation.hpp"

Sample get_sample(const std::vector<wxPoint>& wx_sample);

Point calculate_mean(const Sample& sample);
Sample diff_mean(const Sample& sample);

Point calculate_max_values(const std::vector<Point>& sample);
Sample div_max_values(const Sample& sample);

double
calculate_length(std::size_t index1, std::size_t index2, const Sample& sample);
double distance(const Point& p1, const Point& p2);

Sample
get_representative_points(const Sample& sample, std::size_t m, double length_D);

Sample Transformation::transform_sample(const std::vector<wxPoint>& wx_sample,
                                        std::size_t m)
{
    auto sample            = get_sample(wx_sample);
    auto sample_diff_mean  = diff_mean(sample);
    auto sample_div_by_max = div_max_values(sample_diff_mean);
    auto D =
        calculate_length(0, sample_div_by_max.size() - 1, sample_div_by_max);
    auto representative_sample =
        get_representative_points(sample_div_by_max, m, D);

    return representative_sample;
}

Point calculate_mean(const std::vector<Point>& sample)
{
    Point p(0, 0);
    for (const auto& s : sample) { p += s; }
    p /= sample.size();
    return p;
}

Sample diff_mean(const Sample& sample)
{
    auto mean = calculate_mean(sample);
    Sample diff_sample;
    for (std::size_t i = 0; i < sample.size(); ++i) {
        diff_sample.push_back(sample[i] - mean);
    }

    return diff_sample;
}

Point calculate_max_values(const std::vector<Point>& sample)
{
    Point max = sample[0];
    for (const auto& p : sample) {
        if (max.x < std::abs(p.x)) { max.x = std::abs(p.x); }
        if (max.y < std::abs(p.y)) { max.y = std::abs(p.y); }
    }
    return max;
}

Sample div_max_values(const std::vector<Point>& sample)
{
    auto max_values = calculate_max_values(sample);
    double max      = std::max(max_values.x, max_values.y);

    Sample div_sample;
    for (std::size_t i = 0; i < sample.size(); ++i) {
        div_sample.push_back(sample[i] / max);
    }

    return div_sample;
}

double
calculate_length(std::size_t index1, std::size_t index2, const Sample& sample)
{
    double length = 0;
    for (std::size_t i = index1 + 1; i <= index2; i++) {
        length += distance(sample[i - 1], sample[i]);
    }
    return length;
}

double distance(const Point& p1, const Point& p2)
{
    return std::sqrt(std::pow(p1.x - p2.x, 2) + std::pow(p1.y - p2.y, 2));
}

Sample
get_representative_points(const Sample& sample, std::size_t m, double length_D)
{
    Sample representative_sample;

    for (std::size_t k = 0; k < m; ++k) {
        double d         = k * length_D / (m - 1);
        double min_d     = d;
        double min_index = 0;

        for (std::size_t i = 0; i < sample.size(); ++i) {
            double p_d = calculate_length(0, i, sample);
            if (p_d - d == 0) {
                min_index = i;
                break;
            }
            if (min_d > std::abs(p_d - d)) {
                min_index = i;
                min_d     = std::abs(p_d - d);
            }
        }

        // FIXME later later
        representative_sample.push_back(sample[min_index]);
    }

    return representative_sample;
}

Sample get_sample(const std::vector<wxPoint>& wx_sample)
{
    Sample sample;
    for (const auto& p : wx_sample) { sample.push_back(p); }
    return sample;
}
