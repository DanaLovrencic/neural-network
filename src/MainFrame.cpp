#include "MainFrame.hpp"

MainFrame::MainFrame()
    : wxFrame(nullptr,
              wxID_ANY,
              "Neural Network",
              wxDefaultPosition,
              wxSize{800, 600}),
      drawing_panel(new DrawingPanel(this)),
      menu_panel(new MenuPanel(this, drawing_panel, should_terminate_training))
{
    SetBackgroundColour({20, 20, 20});
    drawing_panel->SetBackgroundColour({20, 20, 20});
    menu_panel->SetBackgroundColour({20, 20, 20});

    auto sizer = new wxBoxSizer(wxVERTICAL);
    sizer->Add(drawing_panel, 1, wxEXPAND | wxLEFT | wxRIGHT, 2);
    sizer->Add(menu_panel, 0, wxEXPAND | wxALL, 10);

    SetSizer(sizer);
}

void MainFrame::on_close_event(wxCloseEvent& event)
{
    should_terminate_training = true;
    wxFrame::OnCloseWindow(event);
}

wxBEGIN_EVENT_TABLE(MainFrame, wxFrame) // clang-format off
    EVT_CLOSE(MainFrame::on_close_event)
wxEND_EVENT_TABLE() // clang-format on
