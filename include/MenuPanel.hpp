#ifndef MENU_PANEL_HPP
#define MENU_PANEL_HPP

#include "DrawingPanel.hpp"
#include "NeuralNetwork.hpp"
#include "Configuration.hpp"

#include <wx/wx.h>

#include <memory>
#include <future>

class MenuPanel : public wxPanel {
  private:
    DrawingPanel* drawing_panel;

    wxButton* train_btn;
    wxButton* predict_btn;
    wxButton* clear_screen_btn;

    Configuration config;

    NeuralNetwork nn;
    std::future<void> train_future;

  public:
    MenuPanel(wxWindow* parent,
              DrawingPanel* drawing_panel,
              std::atomic<bool>& should_terminate_training);

  private:
    void on_predict_btn_pressed(wxCommandEvent& event);
    void on_train_btn_pressed(wxCommandEvent& event);
    void on_clear_screen_btn_pressed(wxCommandEvent& event);
    void on_close_event(wxCloseEvent& event);

    void on_train_finished();

    wxDECLARE_EVENT_TABLE();
};

#endif
