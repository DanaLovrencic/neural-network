#ifndef CONFIGURATION_HPP
#define CONFIGURATION_HPP

#include <vector>

struct Configuration {
    constexpr static std::size_t m          = 30;
    constexpr static std::size_t n_outputs  = 5;
    constexpr static std::size_t n_inputs   = 2 * m;
    constexpr static std::size_t n_iters    = 70000;
    constexpr static std::size_t n_samples  = 100;
    constexpr static std::size_t batch_size = 10;
    constexpr static double epsilon         = 1e-5;
    constexpr static double eta             = 0.5;
    std::vector<unsigned> architecture;

    Configuration();
};

#endif
