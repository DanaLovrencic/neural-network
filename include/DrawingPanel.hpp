#ifndef DRAWING_PANEL_HPP
#define DRAWING_PANEL_HPP

#include "Point.hpp"

// Main window class
class DrawingPanel : public wxPanel {
  private:
    std::vector<std::vector<wxPoint>> samples;
    bool is_down = false; // Record whether the mouse is in a pressed state

  public:
    DrawingPanel(wxWindow* parent);
    void on_left_move(wxMouseEvent& event);
    void on_left_down(wxMouseEvent& event);
    void on_left_up(wxMouseEvent& event);
    void on_paint(wxPaintEvent& event);

    std::vector<wxPoint> get_wx_sample() const;
    void clear();

    // Declaration event table
    DECLARE_EVENT_TABLE()
};

#endif
