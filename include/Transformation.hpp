#ifndef TRANSFORMATION_HPP
#define TRANSFORMATION_HPP

#include "Point.hpp"

#include <vector>
#include <wx/wx.h>

using Sample = std::vector<Point>;

namespace Transformation {
Sample transform_sample(const std::vector<wxPoint>& wx_sample, std::size_t m);

}

#endif
