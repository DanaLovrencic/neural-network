#ifndef POINT_HPP
#define POINT_HPP

#include <wx/wx.h>
#include <vector>

struct Point {
    double x, y;
    Point(const wxPoint& point);
    Point(double x, double y);
    Point() = default;
    Point operator+=(const Point& other);
    Point operator-=(const Point& other);
    Point operator/=(const Point& other);
    Point operator/=(std::size_t num);
    friend std::ostream& operator<<(std::ostream& os, const Point& p);
    friend Point operator+(const Point& first, const Point& other);
    friend Point operator/(const Point& first, const Point& other);
    friend Point operator/(const Point& point, std::size_t num);
    friend Point operator-(const Point& first, const Point& other);
};

std::ostream& operator<<(std::ostream& os, const std::vector<Point>& p);

#endif
